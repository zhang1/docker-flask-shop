#!/usr/bin/env python
import os

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager, Shell, Server
from app import create_app, db
from app.models import User, New, Goods, List, Item
# from redis import Redis
# from rq import Connection, Queue, Worker
# import subprocess

#install Manager, Manager can controll action 
#pass your setting command
app = create_app()
manager = Manager(app)
migrate = Migrate(app, db)

#try to set in another config is best,
#secret_key make session can be protect
# app.secret_key = '123'

#setting command below that, and can use 
# python3 manage.py command to call
#ex: python3 manage.py test
@manager.command
def test():
    """Run the unit tests."""
    print('ok')

#use docker to make database,so data mrgration unnecessary
#if want to use this, first initial
#second, migrate data to migration
manager.add_command('db', MigrateCommand)

#test server have run ,set port 80
manager.add_command("runserver", Server(host='0.0.0.0', port=80))


@manager.command
def recreate_db():
    """
    Recreates a local database. You probably should not use this on
    production.
    """
    db.drop_all()
    db.create_all()
    db.session.commit()

#create all database
@manager.command
def create_all():
    with app.app_context():
        db.create_all()

#add test user
@manager.command
def create_db():
    new_user = User(email = '123@123.com',
                    username = '123456',
                    password = '12345678')
    db.session.add(new_user)
    db.session.commit()

#create admin that can edit post and shop
@manager.command
def create_admin():
    """
    create administer
    """
    check_admin = User.query.filter_by(is_admin=True).first()

    if check_admin:
        return "admin"
    else:
        new_admin = User(email = 'admin@admin.com',
                        username = 'admin',
                        password = '12345678',
                        is_admin = True)
        db.session.add(new_admin)
        db.session.commit()

#see database
@manager.command
def see_db():
    user = User.query.all()
    new = New.query.all()
    good = Goods.query.all()

    print(good)
    print(user)
    print(new)


if __name__ == '__main__':
    manager.run()
