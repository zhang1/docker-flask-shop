import datetime

from flask import (
    Blueprint,
    abort,
    flash,
    redirect,
    render_template,
    request,
    url_for,
    g,
)
from flask_login import current_user, login_required

from app import db
from app.models import Goods, List, Item 


shop = Blueprint('shop', __name__)

@shop.before_request
def before_request():
    goods = Goods.query.all()
    g.three_goods = goods[-3:]

#send data to browser, then show all goods 
@shop.route('/goods_all')
def goods_all():
    goods = Goods.query.all()

    return render_template('shop/goods_all.html', goods=goods)

#user select goods and into goods page
#server get data from database and send it to browserver
# if any one list not complete, allow continue to add item
# if complete of list, make new List and store item
# finally, add data into database  
@shop.route('/goods_all/<name>', methods=["GET", "POST"])
def goods_all_item(name):
    goods = Goods.query.filter_by(name=name).first()
    
    if request.form:
        now = datetime.datetime.now()
        list_current = List.query.filter(List.user_id==current_user.id, List.complete_on==False).all()

        if list_current:

            item_add = Item(
                list_id = list_current[0].id,
                name = goods.name,
                price = goods.price,
                num = request.form['num'],
            )
            
            db.session.add(item_add)
            db.session.commit()

            return redirect(url_for('shop.goods_all')) 

        else: 

            list_add = List(
                user_id = current_user.id,
                buy_date = now.strftime("%Y-%m-%d %H:%M")
            )
            db.session.add(list_add)
            db.session.commit()
            
            item_add = Item(
                list_id = list_add.id,
                name = goods.name,
                price = goods.price,
                num = request.form['num'],
            )
            
            db.session.add(item_add)
            db.session.commit()

            return redirect(url_for('shop.goods_all')) 

    return render_template('shop/goods_item.html', goods=goods)

#server deliver data to browser, that data is belong to current user
#browser get dat and show on page
@shop.route('/order_sheet')
def order_sheet():
    if current_user.is_active:
        shop_list = List.query.filter_by(user_id=current_user.id).all()

    else:
        shop_list = None

    return render_template('shop/order_sheet.html', shop_list=shop_list)

#user select id that user want to checkout or read info
@shop.route('/order_status/<id>', methods=["GET", "POST"])
@login_required
def order_status(id):
    item = Item.query.all()
    order_id_status = Item.query.filter_by(list_id=id).all()

    if request.form:

        if request.form["ok"] == "ok":
            sum = 0

            for i in order_id_status:
                sum += i.price

            current_user.wallet -= sum
            order_id_status[0].list.complete_on = True
            db.session.commit()

        return redirect(url_for('shop.order_sheet'))

    return render_template('shop/order_status.html', order_id_status=order_id_status)
