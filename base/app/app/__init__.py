import os
from datetime import timedelta

from werkzeug.wrappers import BaseRequest, ETagRequestMixin
from werkzeug.utils import secure_filename

from flask import Flask, request, Response, sessions, send_from_directory
from flask_compress import Compress
from flask_login import LoginManager, current_user
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import CsrfProtect

#set object or var, create template, 
#csrf set cookie a random value, and confirm user or not
#if use samesite, don't need to use csrf and install
db = SQLAlchemy()
csrf = CsrfProtect()

# Set up Flask-Login
#session protect from cokie
login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'account.login'


def create_app():
    app = Flask(__name__)

    #can use function os.environ.get(), get setting from env, this 
    # methods is secret, and protect from internet
    app.config.from_pyfile('config.cfg')
    app.config.update(
        SESSION_COOKIE_SECURE=True,
        SESSION_COOKIE_HTTPONLY=True,
        SESSION_COOKIE_SAMESITE='Lax',
        )
    
    #session.save_session(samesite='Lax'), let browser can visit page 
    #with "GET" in Chrome FireFox, samesite can set 'Strict' 
    # set_samesite = sessions.SessionInterface()
    # set_samesite.get_cookie_samesite(app)


    # install extensions, db is database, login_manager can controll
    #user login and logout, detect what current user is, csrf protect
    #cross-site request forgery
    db.init_app(app)
    login_manager.init_app(app)
    csrf.init_app(app)

    #create bluescript
    #make main,admin,shop in another .py for easily manage or dev
    #if into web, send to main first
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .account import account as account_blueprint
    app.register_blueprint(account_blueprint, url_prefix='/account')

    from .shop import shop as shop_blueprint
    app.register_blueprint(shop_blueprint, url_prefix='/shop')

    from .admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint, url_prefix='/admin')


    return app
