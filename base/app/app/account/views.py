from datetime import timedelta

from flask import (
    Blueprint,
    redirect,
    render_template,
    request,
    url_for,
    g,
)
from flask_login import (
    current_user,
    login_required,
    login_user,
    logout_user,
    AnonymousUserMixin,
    UserMixin,
)

from app import db
from app.account.forms import RegisterForm, LoginForm

from ..models import User, Goods
from .. import login_manager


#account.route instead of app.route
account = Blueprint('account', __name__)

#set global user
@account.before_request
def before_request():
    g.user = current_user
    goods = Goods.query.all()
    g.three_goods = goods[-3:]

#set form on website, return form=form, send data to web,
#if subbmit,get data from page
#chectout user exist, psw correct,use login_user()function,
#make user login, redirect home
#set next in env is better, protect path from internet
@account.route('/login', methods=["GET", "POST"])
def login():
    form = LoginForm(request.form)
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()

        if user is not None and user.verify_password(form.password.data):
            login_user(user, remember=True)
            redirect_url = request.args.get('next') or url_for('main.index')

            return redirect(redirect_url)

    return render_template('account/login.html', form=form)

#user logout_user function make user logout
@account.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('account.login'))

#make form on web , and get data from web, then add in database
@account.route('/register', methods=["GET", "POST"])
def register():
    form = RegisterForm(request.form, csrf_enabled=False)
    if form.validate_on_submit():
        new_user = User(email=form.email.data,
                username=form.username.data,
                password=form.password.data)

        db.session.add(new_user)
        db.session.commit()

        return redirect(url_for('account.login'))

    return render_template('account/register.html', form=form)

