from flask import url_for
from flask_wtf import Form
from wtforms import ValidationError, validators
from wtforms.fields import (
    BooleanField,
    PasswordField,
    StringField,
    TextField,
    SubmitField,
)
from wtforms.validators import Email, EqualTo, InputRequired, Length, DataRequired

from app.models import User

#easily make login form in webpage, if prss button on web, get data 
#and checkout data validated from validate function, if not, return
#error in browser
class LoginForm(Form):
    email = TextField('Email',
            validators=[DataRequired(), Length(1, 64), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Keep me logged in')
    submit = SubmitField('Log In')

    #first set **kwargs can call function
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)

    def validate(self):
        #check validate have or not
        initial_validation = super(LoginForm, self).validate()
        if not initial_validation:
            return False

        #check user is None or not, send error to macro display on page
        user = User.query.filter_by(email=self.email.data).first()

        if not user:
            self.email.errors.append('Unknown email')
            return False
        if not user.verify_password(self.password.data):
            self.password.errors.append('Invalid password')
            return False
        return True


class RegisterForm(Form):
    username = TextField('Username',
            validators=[DataRequired(), Length(min=3, max=32)])
    email = TextField('Email',
            validators=[DataRequired(), Email(), Length(min=6, max=40)])
    password = PasswordField('Password',
            validators=[DataRequired(), Length(min=8, max=64)])
    confirm = PasswordField('Verify password',
            validators=[DataRequired(), EqualTo('password',
            message='Passwords must match')])

    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)

    def validate(self):
        initial_validation = super(RegisterForm, self).validate()
        if not initial_validation:
            return False
        #search username from database, if same, return error
        user = User.query.filter_by(username=self.username.data).first()
        if user:
            self.username.errors.append("Username already registered")
            return False
        user = User.query.filter_by(email=self.email.data).first()
        if user:
            self.email.errors.append("Email already registered")
            return False
        return True