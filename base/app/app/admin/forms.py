from flask import url_for
from flask_wtf import Form
from wtforms import ValidationError
from wtforms.fields import (
    BooleanField,
    PasswordField,
    StringField,
    SubmitField,
    DateField,
    TextAreaField,
    TextField,
)

from wtforms.validators import Email, InputRequired, Length, DataRequired

from app.models import New, User

#make NewsEditForm to edit news
class NewsEditForm(Form):
    title = StringField('title', validators=[DataRequired()])
    text = TextAreaField('text', validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        super(NewsEditForm, self).__init__(*args, **kwargs)
    
    def validate(self):
        initial_validation = super(NewsEditForm, self).validate()
        if not initial_validation:
            return False
        return True

#make LoginForm that can easy make form on page
#and make function that check if error or not
class LoginForm(Form):
    email = TextField('Email',
            validators=[DataRequired(), Length(1, 64), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Keep me logged in')

    #first set **kwargs can call function
    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)

    def validate(self):
        #check validate have or not
        initial_validation = super(LoginForm, self).validate()
        if not initial_validation:
            return False

        #check user is None or not, send error to macro display on page
        user = User.query.filter_by(email=self.email.data).first()

        if not user:
            self.email.errors.append('Unknown email')
            return False
        if not user.verify_password(self.password.data):
            self.password.errors.append('Invalid password')
            return False
        return True
