import os, datetime

from flask import (
    Blueprint,
    abort,
    flash,
    redirect,
    render_template,
    request,
    url_for,
    sessions,
    Response,
    send_from_directory,
)
from flask_login import current_user, login_required, login_user, logout_user

from werkzeug.utils import secure_filename

from app.admin.forms import NewsEditForm, LoginForm
from app.models import New, Goods, User
from app import db, create_app
from app.decorators import admin_required

admin = Blueprint('admin', __name__)

#admin login page
#first, checkout if login or not
#if not, browser get form from server depend on method "GET"
# then browser post info to server depend on method "POST" and btn "submit"
#check email if same or not from database
#if exit, verify password, and use login_user to login
@admin.route('/', methods=["POST", "GET"])
def index():
    if current_user.is_active:
        return render_template('admin/admin.html')

    else:
        form = LoginForm(request.form)
        if form.validate_on_submit():
            user = User.query.filter_by(email=form.email.data).first()

            if user is not None and user.verify_password(form.password.data):
                
                login_user(user, remember=True)
                redirect_url = request.args.get('next') or url_for('admin.index')

                return redirect(redirect_url)

        return render_template('admin/admin.html', form=form)

@admin.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('admin.index'))

#get post from database and present post sheet on web, 
#and admin can select what admin want to revise
@admin.route('/post_sheet', methods=["GET", "POST"])
@login_required
@admin_required
def post_sheet():
    post = New.query.all()

    return render_template('admin/post_sheet.html', post=post)

#on web that present post sheet, press del what admin want
#browser send title to server, and get data depend on title
# delete data, and commit modify, send current post data to browser
@admin.route('/post_del/<title>', methods=["GET", "POST"])
@login_required
@admin_required
def post_del(title):
    postdel = New.query.filter_by(title=title).first()
    db.session.delete(postdel)
    db.session.commit()
    post = New.query.all()

    return render_template('admin/post_sheet.html', post=post)

#on page that show post sheet, press edit what admin want
#server get title and according to title grab data from database
#send data to browser, make form depend on data
#after modification, server acuire data and save 
@admin.route('/post_renew/<title>', methods=["GET", "POST"])
@login_required
@admin_required
def post_renew(title):
    post = New.query.filter_by(title=title).first()

    if request.form:
        post.title = request.form["title"]
        post.text = request.form["text"]
        db.session.commit()

        return redirect(url_for('admin.post_sheet'))

    return render_template('admin/post_renew.html', post=post)

#set var form and now, send it to browser
#server obtain title and content,
#add that to database
@admin.route('/post_addnew', methods=["GET", "POST"])
@login_required
@admin_required
def post_addnew():
    form = NewsEditForm(request.form)
    now = datetime.datetime.now()

    if form.validate_on_submit():

        new = New(
            title=form.title.data,
            text=form.text.data,
            edit_date=now.strftime("%Y-%m-%d %H:%M")
            )

        db.session.add(new)
        db.session.commit()

        return redirect(url_for('admin.post_sheet'))

    return render_template('admin/post_addnew.html', form=form)


#server deliver goods from database to browser
#browser get data and make goods sheet
@admin.route('/goods_sheet', methods=["GET"])
@login_required
@admin_required
def goods_sheet():
    goods = Goods.query.all()

    return render_template('admin/goods_sheet.html', goods=goods)

#admin press del what admin want to remove
#server get name from browser, and search data, then delete
@admin.route('/goods_del/<name>', methods=["GET", "POST"])
@login_required
@admin_required
def goods_del(name):
    goods_del = Goods.query.filter_by(name=name).first()
    db.session.delete(goods_del)
    db.session.commit()
    goods = Goods.query.all()

    return render_template('admin/goods_sheet.html', goods=goods)

#server send data that admin want to browser
#then server get data that admin modified, save in database
@admin.route('/goods_renew/<name>', methods=["GET", "POST"])
@login_required
@admin_required
def goods_renew(name):
    goods = Goods.query.filter_by(name=name).first()

    if request.form:
        goods.name = request.form["name"]
        goods.text = request.form["text"]
        goods.price = request.form["price"]
        db.session.commit()

        return redirect(url_for('admin.goods_sheet'))

    return render_template('admin/goods_renew.html', goods=goods)

#make function that check attachment
#using rsplit make string to list contain 2 item 
#checkout allow list have same name, then return True
def allowed_file(filename):
    ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
    MAX_CONTENT_LENGTH = 16 * 1024 * 1024  # 16MB

    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

#picture url
@admin.route('/goods_upload_img/<filename>')
@login_required
@admin_required
def goods_upload_img(filename):

    return send_from_directory('/app/app/app/static/img', filename)

#if admin send info to server
#first, make var file save file from browser
#then, checkout file if exit or not and verify attachment name
#use secure_filename to regular filename that can be os.path.join
#make file_url to save path of img
# final, make goods and add goods to database 
@admin.route('/goods_upload', methods=["GET", "POST"])
@login_required
@admin_required
def goods_upload():
    if request.method == 'POST':
        file = request.files["file"]

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)

            file.save(os.path.join('/app/app/app/static/img', filename))
            file_url = url_for('admin.goods_upload_img', filename=filename)

            now = datetime.datetime.now()

            goods = Goods(
                name=request.form["name"],
                price=request.form["price"],
                text=request.form["text"],
                upload_date=now.strftime("%Y-%m-%d %H:%M"),
                img_path=file_url,
            )

            db.session.add(goods)
            db.session.commit()       

            return redirect(url_for('admin.goods_sheet'))
    
    return render_template('admin/goods_upload.html')

#admin can see all user, and control delete and edit
@admin.route('/user_sheet', methods=["GET"])
@login_required
@admin_required
def user_sheet():
    user_sheet = User.query.all()

    return render_template("admin/user_sheet.html", user_sheet=user_sheet)

#if admin press del, browser send data to server depend on "POST",
#server get data, and search it from database,then delete
@admin.route('/user_del/<username>', methods=["POST", "GET"])
@login_required
@admin_required
def user_del(username):
    user_del  = User.query.filter_by(username=username).first()
    db.session.delete(user_del)
    db.session.commit()

    return redirect(url_for('admin.user_sheet'))

#admin select edit, and into edit page
#server get data, then check email and username same or not in database
#confirm modification
@admin.route('/user_edit/<username>', methods=["POST", "GET"])
@login_required
@admin_required
def user_edit(username):
    user_edit = User.query.filter_by(username=username).first()

    if request.form:
        verify_email = User.query.filter_by(email=request.form["email"]).first()

        if verify_email and user_edit.id != verify_email.id:
            return "email is exit"

        else:
            verify_username = User.query.filter_by(username=request.form["username"]).first()

            if verify_username and user_edit.id != verify_username.id:
                return "username is exit"

            else:
                user_edit.email = request.form["email"]
                user_edit.username = request.form["username"]
                user_edit.password = request.form["password"]
                db.session.commit()

                return redirect(url_for('admin.user_sheet'))
    
    return render_template('/admin/user_edit.html', user_edit=user_edit)