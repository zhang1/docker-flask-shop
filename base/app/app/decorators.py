from functools import wraps
from flask import abort
from flask_login import current_user


#create decorator for controll function, if current_user is 
#addmin, allow function can be execute below @admin_required
def admin_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not current_user.is_admin:
            abort(403)
        return f(*args, **kwargs)
    return decorated
