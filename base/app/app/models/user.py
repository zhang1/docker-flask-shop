from flask import current_app
from flask_login import AnonymousUserMixin, UserMixin

from werkzeug.security import check_password_hash, generate_password_hash
from datetime import datetime

from .. import db, login_manager


#make User template can be save in database
#relationship List
class User(UserMixin, db.Model):
    """build user databases with metadata, include email,username,password,wallet
    """
    __tablename__ = 'users'
    id = db.Column('user_id', db.Integer, primary_key=True)
    email = db.Column('email', db.String(64), unique=True, index=True)
    username = db.Column('username', db.String(64), unique=True)
    password_hash = db.Column('password', db.String(128))
    wallet = db.Column('wallet', db.Integer, default=1000)
    is_admin = db.Column('is_admin', db.Boolean, default=False)
    list_rel = db.relationship('List', backref='users', lazy='dynamic')

    def __repr__(self):
        return '<User({username!r})>'.format(username=self.username)

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

#make List template
#create List when shopping before order checkout
#connect Item
class List(db.Model):
    __tablename__ = 'list'
    id = db.Column('list_id', db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.user_id'))
    item_rel = db.relationship('Item', backref='list', lazy='dynamic')

    complete_on = db.Column(db.Boolean, default=False)
    buy_date = db.Column('buy_date', db.DateTime)

#every list can many item before checkout
#Item template record name, price, num, list_id
class Item(db.Model):
    __tablename__ = 'item'
    id = db.Column('item_id', db.Integer, primary_key=True)

    list_id = db.Column('list_id', db.Integer, db.ForeignKey('list.list_id'))
    
    
    name = db.Column('name', db.String(32))
    price = db.Column('price', db.Integer)
    num = db.Column('num', db.Integer) 


@login_manager.user_loader
def load_user(user_id):
    
    return User.query.get(int(user_id))