from datetime import datetime
from .. import db, login_manager


#new template, like blog can record necessary informtion
#integer like 1, 2
#string like 'that is title'
#DateTime ex 2018-07-13
#uniaue let it different every title
class New(db.Model):
    __tablename__ = 'news'
    id = db.Column('news_id', db.Integer, primary_key=True)
    title = db.Column('title', db.String(64), unique=True)
    text = db.Column('text', db.Text)
    edit_date = db.Column('date', db.DateTime)
    

