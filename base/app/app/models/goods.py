from datetime import datetime
from .. import db


#good template relate to good information, is_display controll
#good present or not
class Goods(db.Model):
    __tablename__ = 'goods'
    id = db.Column('good_id', db.Integer, primary_key=True)
    name = db.Column('name', db.String(64), unique=True)
    price = db.Column('price', db.Integer)
    img_path = db.Column('img_path', db.String(128))
    text = db.Column('text', db.Text)
    upload_date = db.Column('date', db.DateTime)
    is_display = db.Column('is_display', db.Boolean, default=False) 

    
    

