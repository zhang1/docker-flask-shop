from flask import Blueprint, render_template, g
from app.models import New, Goods

#main.route instead of app.route
main = Blueprint('main', __name__)


#make global var that can exit in /main
@main.before_request
def before_request():
    goods = Goods.query.all()
    g.three_goods = goods[-3:]

#my home page url and basic webpage
@main.route('/')
def index():
    post = New.query.all()

    return render_template('main/index.html', post=post)

#client can select title from home page
#server get title and search this data,
#then send it to browser
@main.route('/new_content/<title>')
def new_content(title):
    post = New.query.filter_by(title=title).first()

    return render_template('main/post.html', post=post)
